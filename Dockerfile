FROM python:3.6-alpine
RUN apk -v --update add groff less gcc musl-dev mailcap
RUN pip install --upgrade awscli s3cmd python-magic aws-sam-cli && rm /var/cache/apk/*
WORKDIR /project
COPY . .