# Base libs
import os

# Pip libs
import boto3
import flask
from botocore.config import Config


class DynamoDBService:
    def __init__(self):
        config = Config(connect_timeout=1, read_timeout=1, retries={'max_attempts': 1})
        if os.getenv('ENVIRONMENT', 'dev') != 'production':
            boto3.setup_default_session(profile_name='personal_profile')  # TODO: Move to config
        dynamo_db = boto3.resource('dynamodb', config=config)
        self.champions_table = dynamo_db.Table('ChampionRunes')
        self.users_table = dynamo_db.Table('RuneSharp-Users')

    def get_runes_page_by_champion(self, champion_id):
        try:
            db_response = self.champions_table.get_item(Key={'championId': int(champion_id)})
            runes_page = db_response.get('Item', None)
            return flask.jsonify(runes_page) if runes_page else None
        except Exception:
            return None

    def get_user_by_username(self, username):
        try:
            db_response = self.users_table.get_item(Key={'username': username})
            user = db_response.get('Item', None)
            return user
        except Exception:
            return None
