# Base controller
from controllers.base import BaseController

# Services
from services import DynamoDBService
from helpers import communications


class RunesController(BaseController):
    def __init__(self, api):
        super().__init__(api)
        self.controller_name = 'runes'
        self.dynamo = DynamoDBService()

    def enable(self):
        self.add_endpoint('page', self.get_runes_page, ['GET'], path_param='champion_id')

    def get_runes_page(self, champion_id: int):
        runes_page = self.dynamo.get_runes_page_by_champion(champion_id)
        status_code = 200 if runes_page else 404
        return communications.http_response(status_code, runes_page)
