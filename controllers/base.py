# Pip libs
from flask import Flask


class BaseController:
    def __init__(self, api: Flask):
        self.controller_name = None
        self.api = api
        if api.config.get('public_endpoints', None) is None:
            api.config['public_endpoints'] = []

    def enable(self):
        raise NotImplementedError("The {} must enable it's own endpoints.".format(self.__class__.__name__))

    def add_endpoint(self, endpoint_name, endpoint_function, available_methods, path_param=None, public=False):
        if not self.controller_name:
            raise NotImplementedError("The {} must have a self.controller_name.".format(self.__class__.__name__))
        if path_param:
            endpoint_path = '/{}/{}/<{}>'.format(self.controller_name, endpoint_name, path_param)
        else:
            endpoint_path = '/{}/{}'.format(self.controller_name, endpoint_name)
        if public:
            self.api.config['public_endpoints'].append(endpoint_name)
        self.api.add_url_rule(endpoint_path, endpoint_name, endpoint_function, methods=available_methods)
