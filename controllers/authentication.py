# Pip libs
from flask import request
from werkzeug.security import check_password_hash

# Base controller
from controllers.base import BaseController

# Services
from services import DynamoDBService

# Helpers
from helpers.communications import jwt_is_valid, http_response, issue_new_jwt


class AuthController(BaseController):
    def __init__(self, api):
        super().__init__(api)
        self.controller_name = 'auth'
        self.api.before_request(self.before_req)
        self.dynamo = DynamoDBService()

    def enable(self):
        self.add_endpoint('login', self.login, ['POST'], public=True)

    def before_req(self):
        public_endpoints = self.api.config['public_endpoints']
        endpoint_is_public = request.endpoint in public_endpoints
        jwt_token = request.headers.get('Authorization')
        authorized = jwt_is_valid(jwt_token) or endpoint_is_public
        if not authorized:
            return http_response(401, {'message': 'You\'re not authorized to perform this action.'})

    def login(self):
        body = request.json
        try:
            user = self.dynamo.get_user_by_username(body['username'])
            if not user or not check_password_hash(user['password'], body['password']):
                return http_response(401, {'message': 'Invalid credentials'})
            else:
                token = issue_new_jwt(user)
                return http_response(200, {'accessToken': token.decode('utf-8')})
        except (KeyError, TypeError):
            return http_response(400, {'message': 'Required fields not sent'})
