# Pip libs
from flask import make_response, jsonify
import jwt
import time
import os


def jwt_is_valid(token: str):
    try:
        payload = jwt.decode(token, os.getenv('JWT_KEY'))
        return payload
    except jwt.DecodeError:
        return False


def http_response(status_code: int, body: dict = None, json: bool = False):
    headers = {'Content-Type': 'application/json'}
    if body:
        return make_response(jsonify(body) if json else body, status_code, headers)
    return make_response({}, status_code)


def issue_new_jwt(user):
    jwt_token = jwt.encode(
        {
            'username': user['username'],
            'iat': int(time.time())
        },
        os.getenv('JWT_KEY')
    )
    return jwt_token
