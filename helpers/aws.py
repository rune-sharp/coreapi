# Base libs
import decimal

# Pip libs
import flask


# Helper class to convert a DynamoDB item to JSON.
class DynamoDecimalEncoder(flask.json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if abs(o) % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DynamoDecimalEncoder, self).default(o)
