# Pip libs
import awsgi
from flask import Flask

# Controllers
from controllers import AuthController, RunesController

# Helpers
from helpers.aws import DynamoDecimalEncoder

api = Flask("Rune# API")
api.json_encoder = DynamoDecimalEncoder
AuthController(api).enable()
RunesController(api).enable()


def lambda_handler(event, context):
    return awsgi.response(api, event, context, base64_content_types={"image/png"})


if __name__ == '__main__':
    api.run(debug=True)
